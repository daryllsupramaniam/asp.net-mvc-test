﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVCCoreWebApp.Models;
using MVCCoreWebApp.Models.Context;

namespace MVCCoreWebApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly TestContext _context;

        public AccountController(TestContext context) //SAMPLE NG D.I sa Constructor
        {
            _context = context;
        }


        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(User user)
        {
            if (_context.Users.Any(x => x.UserCode == user.UserCode && x.UserPassword == user.UserPassword))
            {
                HttpContext.Session.SetString("_User", user.UserCode);
                HttpContext.Session.SetString("_Password", user.UserPassword);

                return Redirect("~/Home");
            }
            else
            {
                ViewBag.Error = "User or Password incorrect";
                return View();
            }
        }
    }
}