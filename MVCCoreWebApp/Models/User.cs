﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVCCoreWebApp.Models
{
    public class User
    {
        public int Id { get; set; }

        [Display(Name ="User Code")]
        [Required]

        public string UserCode { get; set; }

        [Display(Name = "Password")]
        [Required]
        public string UserPassword { get; set; }
    }
}
